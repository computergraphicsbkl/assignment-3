﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Area_fill
{
    public partial class Form1 : Form
    {
        Color BorderColor = Color.Black;
        Color FillColor = Color.Red;
        bool isPressed = false;
        Point CurrentPoint;
        Point PreviousPoint;
        Graphics g;
        bool mode = true;  // true — draw mode; false — fill mode.
        const string PATH = "../../background.bmp";
        Bitmap image;

        public Form1()
        {
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = Image.FromFile(PATH);
            image = new Bitmap(pictureBox1.Image);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = colorDialog1.ShowDialog();
            if (DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                BorderColor = colorDialog1.Color;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clear_picturebox();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            mode = true;
            button3.Enabled = false;
            button4.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            mode = false;
            button3.Enabled = true;
            button4.Enabled = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DialogResult = colorDialog2.ShowDialog();
            if (DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                FillColor = colorDialog2.Color;
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            isPressed = false;
            pictureBox1.Image = image;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPressed)
            {
                PreviousPoint = CurrentPoint;
                CurrentPoint = e.Location;
                paint();
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (mode)
            {   // Режим рисования
                isPressed = true;
                CurrentPoint = e.Location;
            }
            else
            {   // Режим заливки
                scan_line_fill(new Point(e.Location.X, e.Location.Y));
                //simple_fill(e.Location.X, e.Location.Y);
                pictureBox1.Image = image;
            }
        }

        private void paint()
        {
            try
            {
                image.SetPixel(CurrentPoint.X, CurrentPoint.Y, BorderColor);
                bresenham_line(CurrentPoint.X, CurrentPoint.Y, PreviousPoint.X, PreviousPoint.Y);
                pictureBox1.Image = image;
            }
            catch(System.ArgumentOutOfRangeException e)
            {
                DialogResult = MessageBox.Show("Out of range of this area!");
                isPressed = false;
                clear_picturebox();
            } 
        }

        private void clear_picturebox()
        {
            pictureBox1.Image.Dispose();
            image.Dispose();
            pictureBox1.Image = Image.FromFile(PATH);
            image = new Bitmap(pictureBox1.Image);
        }

        // System.Diagnostics.Debug.WriteLine($"GGGGGGG\n");

        private static void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        // Алгоритм Брезенхейма
        void bresenham_line(int x0, int y0, int x1, int y1)
        {
            bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0); // Проверяем рост отрезка по оси икс и по оси игрек
                                                               // Отражаем линию по диагонали, если угол наклона слишком большой
            if (steep)
            {
                Swap(ref x0, ref y0); // Перетасовка координат вынесена в отдельную функцию для красоты
                Swap(ref x1, ref y1);
            }
            // Если линия растёт не слева направо, то меняем начало и конец отрезка местами
            if (x0 > x1)
            {
                Swap(ref x0, ref x1);
                Swap(ref y0, ref y1);
            }
            int dx = x1 - x0;
            int dy = Math.Abs(y1 - y0);
            int error = dx / 2; // Здесь используется оптимизация с умножением на dx, чтобы избавиться от лишних дробей
            int ystep = (y0 < y1) ? 1 : -1; // Выбираем направление роста координаты y
            int y = y0;
            for (int x = x0; x <= x1; x++)
            {
                image.SetPixel(steep ? y : x, steep ? x : y, BorderColor); // Не забываем вернуть координаты на место
                error -= dy;
                if (error < 0)
                {
                    y += ystep;
                    error += dx;
                }
            }
        }

        // Простейший алгоритм рекурсивной заливки
        private void simple_fill(int x, int y)
        {
            if (x >= 0 && x < image.Width && y >= 0 && y < image.Height &&
                image.GetPixel(x, y).ToArgb() != BorderColor.ToArgb() && image.GetPixel(x, y).ToArgb() != FillColor.ToArgb())
            {
                image.SetPixel(x, y, FillColor);

                simple_fill(x + 1, y);
                simple_fill(x - 1, y);
                simple_fill(x, y + 1);
                simple_fill(x, y - 1);
            }
        }

        // Алгиритм заливки на основе линий (через стек)
        private void scan_line_fill(Point pt)
        {
            Color targetColor = this.image.GetPixel(pt.X, pt.Y);
            if (targetColor.ToArgb().Equals(FillColor.ToArgb()))
            {
                return;
            }

            Queue<Point> pixels = new Queue<Point>();

            pixels.Enqueue(pt);
            while (pixels.Count != 0)
            {
                Point temp = pixels.Dequeue();
                int x1 = temp.X;
                while (x1 >= 0 && image.GetPixel(x1, temp.Y) == targetColor)
                {
                    x1--;
                }
                x1++;
                bool spanUp = false;
                bool spanDown = false;
                while (x1 < image.Width && image.GetPixel(x1, temp.Y) == targetColor)
                {
                    image.SetPixel(x1, temp.Y, FillColor);

                    if (!spanUp && temp.Y > 0 && image.GetPixel(x1, temp.Y - 1) == targetColor)
                    {
                        pixels.Enqueue(new Point(x1, temp.Y - 1));
                        spanUp = true;
                    }
                    else if (spanUp && temp.Y > 0 && image.GetPixel(x1, temp.Y - 1) != targetColor)
                    {
                        spanUp = false;
                    }
                    if (!spanDown && temp.Y < image.Height - 1 && image.GetPixel(x1, temp.Y + 1) == targetColor)
                    {
                        pixels.Enqueue(new Point(x1, temp.Y + 1));
                        spanDown = true;
                    }
                    else if (spanDown && temp.Y < image.Height - 1 && image.GetPixel(x1, temp.Y + 1) != targetColor)
                    {
                        spanDown = false;
                    }
                    x1++;
                }
            }
        }
    }
}
