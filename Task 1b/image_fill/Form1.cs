﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace image_fill
{
    public partial class Form1 : Form
    {
        //for main image
        private Graphics g;
        private Bitmap image;

        //for image to be filled with
        private Graphics g2;
        private Bitmap image2;

        //pixel storage
        private int xG, yG;

        //current pixel storage
        private Color curPixel;

        public Form1()
        {
            InitializeComponent();
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void LoadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Image Files(*.BMP;*.JPG;*.JPEG;*.GIF;*.PNG)|*.BMP;*.JPG;*.JPEG;*.GIF;*.PNG|All files (*.*)|*.*";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    image = new Bitmap(openDialog.FileName);
                    pictureBox1.Image = image;
                    g = Graphics.FromImage(pictureBox1.Image);
                    pictureBox1.Invalidate();
                    Form1.ActiveForm.Width = image.Width + 70;
                    Form1.ActiveForm.Height = image.Height + menuStrip1.Height + panel1.Height;
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void LoadTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Image Files(*.BMP;*.JPG;*.JPEG;*.GIF;*.PNG)|*.BMP;*.JPG;*.JPEG;*.GIF;*.PNG|All files (*.*)|*.*";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    image2 = new Bitmap(openDialog.FileName);
                    pictureBox2.Image = image2;
                    g2 = Graphics.FromImage(pictureBox2.Image);
                    pictureBox2.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {

                if (pictureBox2.Image == null)
                {
                    MessageBox.Show("Выберите шаблон");
                }

                FillWithTemplate(xG, yG, 0, curPixel);
                pictureBox1.Refresh();

            }
            else
            {
                MessageBox.Show("Основное изображение не выбрано",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FillWithTemplate(int xImg, int yImg, int yTemplate, Color oldColor)
        {
           int xLeft = xImg - 1, xRight = xImg + 1;

            if (image.GetPixel(xImg, yImg).ToArgb() != oldColor.ToArgb())
                return;

            Color pix;

            //get left pixel color
            if (xLeft != -1)
            {
                pix = image.GetPixel(xLeft, yImg);

                while (xLeft >= 0 && pix == oldColor)
                {
                    xLeft--;
                    if (xLeft >= 0)
                        pix = image.GetPixel(xLeft, yImg);
                }
            }

            pix = image.GetPixel(xRight, yImg);

            // get right pixel color
            if (xRight != image.Width)
            {
                pix = image.GetPixel(xRight, yImg);

                while (xRight < image.Width && pix == oldColor)
                {
                    xRight++;
                    if (xRight < image.Width)
                        pix = image.GetPixel(xRight, yImg);
                }
            }


            // draw line
            int newX;
            newX = image2.Width - (xG - xLeft) % image2.Width;

            if (yTemplate == -1)
                yTemplate = image2.Height - 1;
            ++xLeft;
            --xRight;

            for (int i = xLeft; i < xRight; ++i)
            {
                image.SetPixel(i, yImg, image2.GetPixel((image2.Width / 2 + newX++) % image2.Width, /*(image2.Height / 2 + yTemplate) % image2.Height*/ yTemplate % image2.Height));
            }

            // ---

            for (int i = xLeft; i < xRight; ++i)
            {
                if (yImg < pictureBox1.Image.Height - 1)
                    FillWithTemplate(i, yImg + 1, yTemplate + 1, oldColor);
                if (yImg > 0)
                    FillWithTemplate(i, yImg - 1, yTemplate - 1, oldColor);
            }
        }

        private void PictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            //select a pixel to start filling from
            if (pictureBox1.Image != null)
            {
                xG = e.Location.X;
                yG = e.Location.Y;
                curPixel = (pictureBox1.Image as Bitmap).GetPixel(e.Location.X, e.Location.Y);
                label2.Text = xG.ToString();
                label3.Text = yG.ToString();
            }

        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (pictureBox1.Image != null && e.Button == MouseButtons.Left)
                DrawLineOnAPicture(e);
        }

        private void DrawLineOnAPicture(MouseEventArgs e)
        {
            g.DrawLine(new Pen(Color.White, trackBar1.Value), new Point(xG, yG), e.Location);
            xG = e.Location.X;
            yG = e.Location.Y;
            pictureBox1.Refresh();
        }

        private void PictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            //select a pixel to start pencil drawing from
            xG = e.Location.X;
            yG = e.Location.Y;
        }
    }
}
