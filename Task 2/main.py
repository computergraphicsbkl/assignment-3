from tkinter import *
from tkinter import filedialog
from PIL import Image, ImageTk
from PIL.ImageOps import invert
from time import sleep
import os.path

def dialog_open():
    return filedialog.askopenfilename(initialdir="~", filetypes=[("BMP", "*.bmp"), ("PNG", "*.png"),
                                                                 ("All files", "*.*")])

def file_open():
    global image
    try:
        scale = int(scale_text.get())
    except ValueError:
        return
    if scale < 1:
        return
    filename = dialog_open()
    if filename == ():
        return
    if not os.path.isfile(filename):
        return
    root.title(filename + " - HSV")
    image = Image.open(filename)
    show_image(image, scale)

def show_image(image, scale):
    global imagetk
    if image is None:
        return
    imagetk = ImageTk.PhotoImage(image.resize((image.width * scale, image.height * scale)))
    canvas.delete("all")
    canvas.create_image(0, 0, image=imagetk, anchor=NW)

def process():
    if image is None:
        return
    try:
        r = int(r_text.get())
        g = int(g_text.get())
        b = int(b_text.get())
    except ValueError:
        return
    boundary = boundary_tracing(image, (r, g, b))
    show_boundary(image, boundary)

def get_pixel(image, x, y, exception_color):
    pixels = image.load()
    if 0 <= x < image.width and 0 <= y < image.height:
        return pixels[x, y]
    else:
        return exception_color

def boundary_tracing(image, boundary_color):
    exception_color = (255 - boundary_color[0], 255 - boundary_color[1], 255 - boundary_color[2])
    boundary = []
    start_pixel = None
    backtrack_pixel = (-1, image.height)
    for y in range(image.height - 1, -1, -1):
        if start_pixel is not None:
            break
        for x in range(image.width):
            if get_pixel(image, x, y, exception_color) == boundary_color:
                start_pixel = (x, y)
                break
            else:
                backtrack_pixel = (x, y)
    if start_pixel is None:
        return boundary
    boundary.append(start_pixel)
    boundary_pixel = start_pixel
    current_pixel = next_clockwise(boundary_pixel, backtrack_pixel)
    while current_pixel != start_pixel:
        current_pixel_color = get_pixel(image, current_pixel[0], current_pixel[1], exception_color)
        if current_pixel_color == boundary_color:
            boundary.append(current_pixel)
            backtrack_pixel = boundary_pixel
            boundary_pixel = current_pixel
            current_pixel = next_clockwise(boundary_pixel, backtrack_pixel)
        else:
            backtrack_pixel = current_pixel
            current_pixel = next_clockwise(boundary_pixel, backtrack_pixel)
    return boundary

def next_clockwise(current_pixel, backtrack_pixel):
    difference = (backtrack_pixel[0] - current_pixel[0], backtrack_pixel[1] - current_pixel[1])
    clockwise = [(-1,-1), (0,-1), (1,-1), (1,0), (1,1), (0,1), (-1,1), (-1,0)]
    offset = clockwise[(clockwise.index(difference) + 1) % len(clockwise)]
    new_pixel = (current_pixel[0] + offset[0], current_pixel[1] + offset[1])
    return new_pixel

def show_boundary(image, boundary):
    if boundary == []:
        return
    try:
        scale = int(scale_text.get())
    except ValueError:
        return
    if scale < 1:
        return
    image = image.copy()
    pixels = image.load()
    try:
        delay = float(delay_text.get())
    except ValueError:
        return
    for pixel in boundary:
        pixels[pixel[0], pixel[1]] = (255, 0, 0)
        if (delay > 0):
            show_image(image, scale)
            root.update()
            sleep(delay)
    if (delay == 0):
        show_image(image, scale)

def scale_changed():
    if image is None:
        return
    try:
        scale = int(scale_text.get())
    except ValueError:
        return
    if scale < 1:
        return
    show_image(image, scale)

root = Tk()
root.geometry("1200x800")
root.title("Boundary tracing")
root.update()

delay_text = StringVar(value="0.01")
r_text = StringVar(value="0")
g_text = StringVar(value="0")
b_text = StringVar(value="0")
scale_text = StringVar(value="3")
scale_text.trace("w", lambda name, index, mode: scale_changed())
image = None

button_open = Button(root, text="Open", command=file_open)
label_color = Label(root, text="Boundary color RGB")
entry_r = Entry(root, textvariable=r_text)
entry_g = Entry(root, textvariable=g_text)
entry_b = Entry(root, textvariable=b_text)
label_scale = Label(root, text="Scale")
entry_scale = Entry(root, textvariable=scale_text)
label_delay = Label(root, text="Delay")
entry_delay = Entry(root, text="0.1", textvariable=delay_text)
button_process = Button(root, text="Process", command=process)
canvas = Canvas(root, width=root.winfo_width(), height=root.winfo_height()-30)

button_open.grid(row=0, column=0, padx=(2, 0))
label_color.grid(row=0, column=1, padx=(2, 0))
entry_r.grid(row=0, column=2, padx=(2, 0))
entry_g.grid(row=0, column=3, padx=(2, 0))
entry_b.grid(row=0, column=4, padx=(2, 0))
label_scale.grid(row=0, column=5, padx=(2, 0))
entry_scale.grid(row=0, column=6, padx=(2, 0))
label_delay.grid(row=0, column=7, padx=(2, 0))
entry_delay.grid(row=0, column=8, padx=(2, 0))
button_process.grid(row=0, column=9, padx=(2, 0))
canvas.grid(row=1, column=0, columnspan=10)

root.mainloop()